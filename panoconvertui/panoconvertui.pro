QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../include/
LIBS=-lstdc++ -lGLEW -lglfw -lrt -lm -ldl -lXrandr -lXinerama \
    -lXi -lXcursor -lXrender -lGL -lm -ldl -ldrm -lXdamage \
    -lXfixes -lX11-xcb -lxcb-glx -lxcb-dri2 -lXxf86vm -lXext -lX11 \
    -lpthread -lxcb -lXau -lXdmcp -lpng -lboost_program_options -ljpeg -lexiv2



SOURCES += \
    main.cpp \
    uiwindow.cpp \
    ../src/pngconverter.cpp \
    ../src/convertwithgl.cpp \
    ../src/image_handling.cpp \
    ../src/pngfile.cpp \
    ../src/jpgfile.cpp \
    ../src/exifinjector.cpp

HEADERS += \
    uiwindow.h \
    pngconverter.h \
    convertwithgl.hpp \
    sourcetypes.hpp \
    image_handling.hpp \
    pngfile.h \
    jpgfile.h \
    exifinjector.hpp

FORMS += \
    uiwindow.ui

TRANSLATIONS += \
    panoconvertui_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

OTHER_FILES +=../src/shaders/*
