#include "uiwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QTimer>
#include <QObject>
#include <boost/program_options.hpp>
#include <string>
#include <iostream>
#include "pngconverter.h"
#include "sourcetypes.hpp"

UIWindow* g_ui;

void xChanged(float value){
    g_ui->setXAxisValue(value);
}

void yChanged(float value){
    g_ui->setYAxisValue(value);
}


void zChanged(float value){
    g_ui->setZAxisValue(value);
}


void foVChanged(float value){
    g_ui->setFoVValue(value);
}

void sourceTypeChanged(SourceType sourceType){
    g_ui->setSourceType(sourceType);
}

void lLensRotationChanged(float value){
    g_ui->setLLensRotationValue(value);
}

void rLensRotationChanged(float value){
    g_ui->setRLensRotationValue(value);
}

int main(int argc, char *argv[])
{
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("ifile", po::value<std::string>(), "input file")
        ("ofile", po::value<std::string>(), "output file")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
    }

    if (!vm.count("ifile") || !vm.count("ofile")) {
        std::cout << "You need to set input file -ifile and output file -ofile parameters" << '\n';
        return 1;
    }
    QApplication a(argc, argv);
    start(argc,argv, vm["ifile"].as<std::string>().c_str());

    QTimer* t = new QTimer();
    QObject::connect(t, &QTimer::timeout, [] {idleFunc();});
    t->start(0);;

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "panoconvertui_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }
    UIWindow w;
    g_ui =&w;
    w.setInFileName(vm["ofile"].as<std::string>());
    w.setOutFileName(vm["ofile"].as<std::string>().c_str());
    setXChangedCallback(xChanged);
    setYChangedCallback(yChanged);
    setZChangedCallback(zChanged);
    setFoVChangedCallback(foVChanged);
    setLLensRotationCallback(lLensRotationChanged);
    setRLensRotationCallback(rLensRotationChanged);
    setSourceTypeCallback(sourceTypeChanged);
    w.show();
    int result = a.exec();
    quit();
    return result;
}
