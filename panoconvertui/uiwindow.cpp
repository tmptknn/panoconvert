#include "uiwindow.h"
#include "ui_uiwindow.h"
#include "pngconverter.h"
#include "sourcetypes.hpp"
#include <string>
#include <math.h>
#include <QFileDialog>

UIWindow::UIWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::UIWindow)
{
    ui->setupUi(this);
    setButtonImage(ui->pushButton_horizontal_upup, "icons/icon_upup.png");
    setButtonImage(ui->pushButton_horizontal_rightup_leftMirrored, "icons/icon_upright_leftmirrored.png");
    setButtonImage(ui->pushButton_sourceSingleFishEye_upfront, "icons/icon_single.png");
    setButtonImage(ui->pushButton_topofother_upup, "icons/icon_topofother_upup.png");
}

UIWindow::~UIWindow()
{
    delete ui;
}

void UIWindow::setButtonImage(QPushButton* button, QString imagePath){
    QPixmap pixmap(imagePath);
    QIcon ButtonIcon(pixmap);
    button->setIcon(ButtonIcon);
    QSize originalSize = pixmap.rect().size();
    button->setIconSize(QSize(originalSize.width()/8,originalSize.height()/8));
}

void UIWindow::setOutFileName(std::string fileName){
    outFileName = fileName;
}

void UIWindow::setInFileName(std::string fileName){
    originalFileName = fileName;
}


void UIWindow::setXAxisValue(float value){
    setValue(value,ui->lineEdit_Xaxis, ui->horizontalSlider_Xaxis);
    setXOffset(value);
}

void UIWindow::setYAxisValue(float value){
    setValue(value,ui->lineEdit_Yaxis, ui->horizontalSlider_Yaxis);
    setYOffset(value);
}

void UIWindow::setZAxisValue(float value){
    setValue(value,ui->lineEdit_Zaxis, ui->horizontalSlider_Zaxis);
    setZOffset(value);
}

void UIWindow::setFoVValue(float value){
    setValue(value,ui->lineEdit_FoV, ui->horizontalSlider_FoV);
    setFoV(value);
}

void UIWindow::setLLensRotationValue(float value){
    setValue(value*100, ui->lineEdit_lLensRotation, ui->horizontalSlider_lLensRotation);
    setLLensRotation(value);
}

void UIWindow::setRLensRotationValue(float value){
   setValue(value*100, ui->lineEdit_rLensRotation, ui->horizontalSlider_rLensRotation);
   setRLensRotation(value);
}

void UIWindow::setSourceType(SourceType type){
    if(currentSourceType != type){
        setProgramSourceType(currentSourceType=type);
    }
}

void UIWindow::setValue(float value, QLineEdit* lineEdit, QSlider* slider){
    lineEdit->setText(lineEdit->text().setNum(value));
    int assignValue = int(value*10000.0);
    if(slider->value() != assignValue) slider->setValue(assignValue);
}

void UIWindow::on_lineEdit_FoV_editingFinished()
{
    float value = std::stof(ui->lineEdit_FoV->text().toStdString());
    setFoVValue(value);
}


void UIWindow::on_horizontalSlider_FoV_sliderMoved(int position)
{
    float value = float(position)/10000.0f;
    setFoVValue(value);
}



void UIWindow::on_horizontalSlider_Zaxis_sliderMoved(int position)
{
    float value = float(position)/10000.0f;
    setZAxisValue(value);
}


void UIWindow::on_lineEdit_Zaxis_editingFinished()
{
    float value = std::stof(ui->lineEdit_FoV->text().toStdString());
    setXAxisValue(value);
}


void UIWindow::on_horizontalSlider_Yaxis_sliderMoved(int position)
{
    float value = float(position)/10000.0f;
    setYAxisValue(value);
}


void UIWindow::on_lineEdit_Yaxis_editingFinished()
{
    float value = std::stof(ui->lineEdit_Yaxis->text().toStdString());
    setYAxisValue(value);
}


void UIWindow::on_horizontalSlider_Xaxis_sliderMoved(int position)
{
    float value = float(position)/10000.0f;
    setXAxisValue(value);
}


void UIWindow::on_lineEdit_Xaxis_editingFinished()
{
    float value = std::stof(ui->lineEdit_Xaxis->text().toStdString());
    setXAxisValue(value);
}


void UIWindow::on_actionOpen_File_triggered()
{
    originalFileName = QFileDialog::getOpenFileName(this->parentWidget(),"Open File",".","*.png *.jpg *.jpeg",nullptr,QFileDialog::Options(QFileDialog::DontUseNativeDialog)).toStdString();
    load(originalFileName.c_str());
    //.setOption(QFileDialog::DontUseNativeDialog,true)
}


void UIWindow::on_actionSave_File_triggered()
{
    save(QFileDialog::getSaveFileName(this->parentWidget(),tr("Save to file"),".",tr("*.png *.jpg *jpeg"),nullptr,QFileDialog::Options(QFileDialog::DontUseNativeDialog)).toStdString(), originalFileName);
}


void UIWindow::on_pushButton_horizontal_upup_clicked()
{
    setSourceType(SourceType::TwoFishEyeParallelUpUp);
}


void UIWindow::on_pushButton_sourceSingleFishEye_upfront_clicked()
{
    setSourceType(SourceType::OneFishEye);
}


void UIWindow::on_pushButton_horizontal_rightup_leftMirrored_clicked()
{
    setSourceType(SourceType::TwoFishEyeParallelUpRightLeftMirrored);
}


void UIWindow::on_pushButton_equilateral_clicked()
{
    setSourceType(SourceType::Equirectangular);
}


void UIWindow::on_pushButton_topofother_upup_clicked()
{
    setSourceType(SourceType::TwoFishEyeOneOnTopOther);
}


void UIWindow::on_lineEdit_lLensRotation_editingFinished()
{
    float value = std::stof(ui->lineEdit_lLensRotation->text().toStdString());
    setLLensRotationValue(value);
}


void UIWindow::on_lineEdit_rLensRotation_editingFinished()
{
    float value = std::stof(ui->lineEdit_rLensRotation->text().toStdString());
    setRLensRotationValue(value);
}


void UIWindow::on_horizontalSlider_lLensRotation_valueChanged(int value)
{
    float valueToBeSet = float(value)/1000000.0;
    setLLensRotationValue(valueToBeSet);
}


void UIWindow::on_horizontalSlider_rLensRotation_valueChanged(int value)
{
    float valueToBeSet = float(value)/1000000.0;
    setRLensRotationValue(valueToBeSet);
}

