#ifndef UIWINDOW_H
#define UIWINDOW_H

#include "sourcetypes.hpp"
#include <QMainWindow>
#include <QLineEdit>
#include <QSlider>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class UIWindow; }
QT_END_NAMESPACE

class UIWindow : public QMainWindow
{
    Q_OBJECT

public:
    UIWindow(QWidget *parent = nullptr);
    ~UIWindow();

    void setOutFileName(std::string fileName);
    void setInFileName(std::string fileName);
    void setXAxisValue(float value);
    void setYAxisValue(float value);
    void setZAxisValue(float value);
    void setFoVValue(float value);
    void setLLensRotationValue(float value);
    void setRLensRotationValue(float value);
    void setSourceType(SourceType type);
private slots:
    void on_lineEdit_FoV_editingFinished();

    void on_horizontalSlider_FoV_sliderMoved(int position);

    void on_horizontalSlider_Zaxis_sliderMoved(int position);

    void on_lineEdit_Zaxis_editingFinished();

    void on_horizontalSlider_Yaxis_sliderMoved(int position);

    void on_lineEdit_Yaxis_editingFinished();

    void on_horizontalSlider_Xaxis_sliderMoved(int position);

    void on_lineEdit_Xaxis_editingFinished();

    void on_actionOpen_File_triggered();

    void on_actionSave_File_triggered();

    void on_pushButton_horizontal_upup_clicked();

    void on_pushButton_sourceSingleFishEye_upfront_clicked();

    void on_pushButton_horizontal_rightup_leftMirrored_clicked();

    void on_pushButton_equilateral_clicked();

    void on_pushButton_topofother_upup_clicked();

    void on_lineEdit_lLensRotation_editingFinished();

    void on_lineEdit_rLensRotation_editingFinished();

    void on_horizontalSlider_lLensRotation_valueChanged(int value);

    void on_horizontalSlider_rLensRotation_valueChanged(int value);

private:
    Ui::UIWindow *ui;
    std::string outFileName = "default.png";
    std::string originalFileName = "test.jpg";
    SourceType currentSourceType = SourceType::TwoFishEyeParallelUpUp;

    void setValue(float value, QLineEdit* lineEdit, QSlider* slider);
    void setButtonImage(QPushButton* button, QString imagePath);

};
#endif // UIWINDOW_H
