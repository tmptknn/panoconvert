#!/bin/bash

for tiedosto in *.JPG;
    do temp=$(basename $tiedosto .JPG)
    convert $tiedosto  $temp.png
    ../../bin/pngconv $temp.png $temp.pano.png
    convert $temp.pano.png $temp.pano.jpg
    exiftool -ProjectionType="equirectangular" $temp.pano.jpg
    done