# Project PanoConvert

converts fisheye images to equirectangle panoramas


## Installation

On Linux install at least these dependencies with apt

libpng-dev 
libglfw3-dev
libglew-dev 
libxi-dev 
libxinerama-dev
libdrm-dev
libxau-dev
libxdamage-dev
libxcb-glx0-dev
libxcb-dri2-0-dev
libxxf86vm-dev 
libavcodec-dev
libavformat-dev
libswscale-dev 
libavfilter-dev

Can be done with 

> sudo apt-get install libpng-dev libglfw3-dev libglew-dev libxi-dev libxinerama-dev libdrm-dev libxau-dev libxdamage-dev libxcb-glx0-dev libxcb-dri2-0-dev libxxf86vm-dev libavcodec-dev libavformat-dev libswscale-dev libavfilter-dev


After dependencies are installed

> cd src

> make all



## Usage

### Convert image to equirectangle

> pngconv fisheye_image.png equirectangle_image.png
opens preview. Conversion is done after escape is pressed.
To rotate image on all axis left, right, up, down, pageup, and pagedown.
Increase fisheye image angle with n and decrease image angle with m.
Escape exits and saves image to given file.

### Convert to jpg

>convert equirectangle_image.png equirectangle_image.jpg

### Add exif information

> exiftool -ProjectionType=equirectangular -FullPanoWidthPixels'<$ImageWidth' -FullPanoHeightPixels'<$ImageHeight' -CroppedAreaImageWidthPixels'<$ImageWidth' -CroppedAreaImageHeightPixels'<$ImageHeight' -CroppedAreaLeftPixels'<$ImageWidth' -CroppedAreaTopPixels=0 -UsePanoramaViewer=True equirectangle_image.jpg
