#ifndef EXIFINJECTOR_HPP
#define EXIFINJECTOR_HPP
#include <string>

#include <exiv2/exiv2.hpp>
#include "image_handling.hpp"
class ExifInjector{

    public:
        void readDataFromFile(std::string fileName);
        void writeDataToFile(std::string fileName, IMAGE_INFO imageInfo);
    private:
        Exiv2::ExifData exifData;
        Exiv2::XmpData xmpData;
        Exiv2::IptcData iptcData;
};


#endif
