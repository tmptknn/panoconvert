#ifndef CONVERTWITHGL_H
#define CONVERTWITHGL_H
#include <string>
void _check_gl_error(const char *file, int line);

///
/// Usage
/// [... some opengl calls]
/// glCheckError();
///
#define check_gl_error() _check_gl_error(__FILE__, __LINE__)
class GLWindowContext{
    public:
    GLuint vbuffer;
    GLuint tbuffer;
    GLuint TextureID;
    GLuint vertexID;
    GLuint coordID;
    GLuint previewTextureID;
    GLuint previewVertexID;
    GLuint previewCoordID;
    GLuint uSizeID;
    GLuint uOffsets;
    GLuint uLensRotation;
    GLuint vao;
    GLuint offScreenTexture;
    GLuint offScreenFrameBuffer;
    GLuint offScreenRenderBuffer;

    GLFWwindow *init(int w, int h, std::string window_name = "Panorama renderer");

    void setupGL();
    GLuint loadProgram(std::string path="", std::string shadername="fragmentshader.glsl");
    GLuint setupPreviewProgram(std::string path="");
    
    GLuint createTexture(unsigned char *data, int width, int height, int outputwidth, int outputheight);

    void addData(unsigned char const **frame, int width, int height, GLuint texture);
    void drawOriginalData(GLuint texture, GLuint program, int width, int height);
    void drawData(GLuint texture, GLuint program, int width, int height,
        float offsetx=0.0f, float offsety=0.0f, float offsetz=0.0f, float offsetw=216.0f,
        float lLensRot =0.0f, float rLensRot=0.0f);
    void previewData(GLuint previewProgram, int width, int height);
    void readInFrame(unsigned char const **frame, int width, int height);

    private:
    GLuint createOffScreenTexture(int width, int height);
    void initBuffers();
    void renderPreview(GLuint texture, GLuint previewProgram);
    void render(GLuint texture, GLuint program, int width, int height, 
            float xoffset = 0.0f, float yoffset = 0.0f, float zoffset = 0.0f, float woffset =0.0f, 
            float lLensRot =0.0f, float rLensRot =0.0f);
};
#endif // GLERROR_H
