#ifndef PNGFILE_H
#define PNGFILE_H
#include "image_handling.hpp"

IMAGE_INFO read_png_file(const char *filename);

void write_png_file(const char *filename, IMAGE_INFO image_info);

#endif