#ifndef __IMAGE_HANDLING_HPP
#define __IMAGE_HANDLING_HPP
#include <string.h>
#include <iostream>

struct IMAGE_INFO{
        unsigned char *data;
        int width;
        int height;
    };

class ImageHandler{
    public:

    ImageHandler();

    IMAGE_INFO loadImage(std::string fileName);

    void saveImage(std::string fileName, IMAGE_INFO info);
    private:
        unsigned char* data;
        int width;
        int height;

};
#endif