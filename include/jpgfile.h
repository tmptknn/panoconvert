#ifndef JPG_FILE_H
#define JPG_FILE_H

#include <jpeglib.h>
#include "image_handling.hpp"

void write_JPEG_file (const char * filename, IMAGE_INFO);
IMAGE_INFO read_JPEG_file (const char * filename);
#endif