#ifndef SOURCETYPES_HPP
#define SOURCETYPES_HPP

enum SourceType{
    Equirectangular = 0,
    TwoFishEyeParallelUpUp = 1,
    TwoFishEyeParallelUpRightLeftMirrored =2,
    TwoFishEyeOneOnTopOther = 3,
    OneFishEye = 10
};


#endif // SOURCETYPES_HPP
