#ifndef PNGCONVERT_H
#define PNGCONVERT_H
#include <string>
#include "sourcetypes.hpp"

void start(int argc, char *argv[],std::string ifilename);

bool running();

void idleFunc();

void setXChangedCallback(void (*func)(float));

void setYChangedCallback(void (*func)(float));

void setZChangedCallback(void (*func)(float));

void setFoVChangedCallback(void (*func)(float));

void setLLensRotationCallback(void (*func)(float));

void setRLensRotationCallback(void (*func)(float));

void setSourceTypeCallback(void (*func)(SourceType));

void setXOffset(float value);

void setYOffset(float value);

void setZOffset(float value);

void setFoV(float value);

void setLLensRotation(float value);

void setRLensRotation(float value);

void setProgramSourceType(SourceType sourceType);

int save(std::string name, std::string original_name);

int load(std::string name);

int quit();
#endif
