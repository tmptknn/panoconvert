#include <string>
#include "sourcetypes.hpp"
#ifndef PARAMETER_PARSER_HPP
#define PARAMETER_PARSER_HPP


class ParameterParser{

    public:
    ParameterParser();

    bool parseParameters(int argc,char **argv, bool requireIFile = true, bool requireOFile= true);

    std::string ifile;
    std::string ofile;
    SourceType inputType;
    bool showOriginal;
    float FoV;
    float xAxisRotation;
    float yAxisRotation;
    float zAxisRotation;
    float lLensRotation;
    float rLensRotation;
    const std::string version = "0.1";
};



#endif