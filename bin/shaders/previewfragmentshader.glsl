#version 330 core
precision highp float;
// Interpolated values from the vertex shaders
in vec2 v_TexCoordinate;

// Ouput data
out vec3 color;

uniform sampler2D uTexture0;

void main ()
{
    color = texture2D(uTexture0, v_TexCoordinate).rgb;
}
