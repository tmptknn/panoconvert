#version 330 core

in vec2 v_TexCoordinate;

uniform vec2 uSize;
uniform vec4 uOffsets;
uniform vec2 uLensRotation;

const float uSetScalar0 = 0.25;
const float uSetScalar1 = 0.75;
const float uSetScalar2 = 0.0;

uniform sampler2D uTexture0;

out vec4 outputColor;

// const vec2 uuSize = vec2(7776,3888);

const float pi = 3.14159265359;
// const float fov = 195.0;

vec3 sphericalToWorld(vec2 sphCoord, float r)
{
  return vec3(
      r * sin(sphCoord.y) * cos(sphCoord.x),
      r * sin(sphCoord.y) * sin(sphCoord.x),
      r * cos(sphCoord.y));
}

vec2 worldToSpherical(vec3 flatCoord, float r)
{
  return vec2(
      atan(flatCoord.x, flatCoord.y),
      acos(flatCoord.z / r));
}

mat3 makeRotationMatrix(vec3 a)
{
  return mat3(
      cos(a.x) * cos(a.z) - sin(a.x) * cos(a.y) * sin(a.z),
      -cos(a.x) * sin(a.z) - sin(a.x) * cos(a.y) * cos(a.z),
      sin(a.x) * sin(a.y),
      sin(a.x) * cos(a.z) + cos(a.x) * cos(a.y) * sin(a.z),
      -sin(a.x) * sin(a.z) + cos(a.x) * cos(a.y) * cos(a.z),
      -cos(a.x) * sin(a.y),
      sin(a.y) * sin(a.z),
      sin(a.y) * cos(a.z),
      cos(a.y));
}

void main()
{
  float fov = uOffsets.w;
  float rotatex = (uOffsets.x+uSetScalar0) * pi * 2.0;
  float rot = (uOffsets.y+uSetScalar1) * pi * 2.0;
  float rotz = (uOffsets.z+uSetScalar2) * pi * 2.0;
  float overlap = (fov - 180.0) / 2.0;
  mat3 rota = makeRotationMatrix(vec3(rotatex, rot, rotz));

  vec2 pL = v_TexCoordinate * vec2(2.0 * pi, 1.0 * pi) - vec2(0.5, 1.0) * pi;
  vec3 worldSphCoordL = sphericalToWorld(pL, 1.0);
  vec3 rotatedWorldSphCoordL = normalize(rota * worldSphCoordL);
  vec2 rotatedSphericalCoordL = worldToSpherical(rotatedWorldSphCoordL, 1.0);

  vec2 coordsL = (rotatedSphericalCoordL + vec2(0.5, 1.0) * pi) * vec2(-1.0 / (1.0 * (2.0 * pi)), 1.0 / (1.0 * pi));

  vec2 pR = vec2(v_TexCoordinate.x, v_TexCoordinate.y) * vec2(2.0 * pi, 1.0 * pi) - vec2(0.5, 0.0) * pi;
  vec3 worldSphCoordR = sphericalToWorld(pR, 1.0);
  vec3 rotatedWorldSphCoordR = normalize(rota * worldSphCoordR);
  vec2 rotatedSphericalCoordR = worldToSpherical(rotatedWorldSphCoordR, 1.0);

  vec2 coordsR = (rotatedSphericalCoordR + vec2(0.5, 0.0) * pi) * vec2(1.0 / (2.0 * pi), 1.0 / (1.0 * pi));

  /*
  vec2 pR = vTexCoord*vec2(1.0*pi,1.0*pi)-1.0*pi;
  vec3 worldSphCoordR = sphericalToWorld(pR,pi/2.0);
  vec3 rotatedWorldSphCoordR =normalize(worldSphCoordR);
  vec2 rotatedSphericalCoordR = worldToSpherical(rotatedWorldSphCoordR, 1.0);
  vec2 coordsR = (rotatedSphericalCoordR+0.5*pi)*vec2(1.0/pi,1.0/(2.0*pi));
  */

  vec2 leftTexCoord = vec2(1.0 - coordsL.x / 2.0, coordsL.y);
  vec2 rightTexCoord = vec2(1.0 - coordsR.x / 2.0 - 0.25, 1. - coordsR.y);
  vec2 uSizeOne = vec2(uSize.x / 2.0, uSize.y);
  // bool left = coordsL.x<0.25;
  vec3 finalColor = vec3(0.0, 0.0, 0.0);
  // float mixvalue = clamp(((vTexCoord.y*360.0-(180-overlap*2.0))/((overlap))),0.0,1.0);
  // float mixvalue = coordsR.y+0.125>0.25*(180/fov)?0.0:1.0;
  for (int k = -1; k < 2; k++)
    for (int l = -1; l < 2; l++)
    {

      vec2 coordinateL = leftTexCoord + vec2(float(k) / (uSizeOne.x * 3.0), float(l) / (uSizeOne.y * 3.0));
      vec2 coordinateR = rightTexCoord + vec2(float(k) / (uSizeOne.x * 3.0), float(l) / (uSizeOne.y * 3.0));
      vec2 scaledL = (1.0 - coordinateL) * vec2(1.0, 2.0);
      vec2 scaledR = (1.0 - coordinateR) * vec2(1.0, 2.0);

      // float rotatedXL = ((scaledXL>rotatex ||left)?(scaledXL-(1.0-rotatex)):(scaledXL-rotatex))*(180/fov) +(180/fov)/4.0;
      // float rotatedXR = ((scaledXR>rotatex )?(scaledXR-(1.0+rotatex)):(scaledXR-rotatex))*(180/fov)-(180/fov)/4.0;
      // float croppedandflippedYL = (1.0-coordinateL.y)*360.0/fov;
      // float croppedandflippedYR = (1.0-coordinateR.y)*360.0/fov;
      vec2 imageCoordsL = scaledL * uSize;
      vec2 imageCoordsR = scaledR * uSize;

      float iL = imageCoordsL.x;
      float jL = imageCoordsL.y;

      float iR = imageCoordsR.x;
      float jR = imageCoordsR.y;

      float xL = (jL / 2.0) * sin((2.0 * pi) * (iL / (uSizeOne.x - 1.0)) - (pi)) + uSizeOne.x / 2.0;

      float yL = (jL / 2.0) * cos((2.0 * pi) * (iL / (uSizeOne.x - 1.0)) - (pi)) + uSizeOne.y / 2.0;

      float xR = (jR / 2.0) * sin((2.0 * pi) * (iR / (uSizeOne.x - 1.0)) - (pi)) + uSizeOne.x / 2.0;
      float yR = (jR / 2.0) * cos((2.0 * pi) * (iR / (uSizeOne.x - 1.0)) - (pi)) + uSizeOne.y / 2.0;

      vec2 panoCoordsL = vec2(xL, yL) / uSize;
      vec2 panoCoordsR = vec2(xR, yR) / uSize;
      vec2 vecToPointL = panoCoordsL; // vec2(panoCoordsL.x*4.0-1.0,panoCoordsL.y*2.0-1.0);
      vec2 vecToPointR = vec2(0.5 - panoCoordsR.x, panoCoordsR.y);
      mat2 lLensRot = mat2(cos(uLensRotation.x),-sin(uLensRotation.x),sin(uLensRotation.x),cos(uLensRotation.x));
      mat2 rLensRot = mat2(cos(uLensRotation.y),-sin(uLensRotation.y),sin(uLensRotation.y),cos(uLensRotation.y));


      vec2 croppedCoordsL = vec2(0.5, 0.0) - (lLensRot*((vecToPointL * vec2(2.0, 1.0) - 0.5)) * (180 / fov) + 0.5) * vec2(0.5, 1.0);
      vec2 croppedCoordsR = (rLensRot*((vecToPointR * vec2(2.0, 1.0) - 0.5)) * (180 / fov) + 0.5) * vec2(0.5, 1.0) + vec2(0.5, 0.0);

      vec4 cameraColorL = texture2D(uTexture0, croppedCoordsL);
      vec4 cameraColorR = texture2D(uTexture0, croppedCoordsR);
      // float mixvalue = length(vecToPointL*vec2(2.0,1.0)-0.5)*2.0<(fov/180)?1.0:0.0;
      float mixvalue = clamp(((length(vecToPointL * vec2(2.0, 1.0) - 0.5) * 1.0 * 360.0 - (180 - overlap / 2.0)) / ((overlap))), 0.0, 1.0);
      finalColor += mix(cameraColorL.rgb, cameraColorR.rgb, mixvalue);
    }
  finalColor /= 9.0;
  outputColor = vec4(finalColor, 1.0);
}