#version 330 core

in vec2 v_TexCoordinate;

uniform vec2 uSize;
uniform vec4 uOffsets;

const float uSetScalar0 = 0.25;
const float uSetScalar1 = 0.0;
const float uSetScalar2 = 0.0;

uniform sampler2D uTexture0;

out vec4 outputColor;

const float pi = 3.14159265359;

vec3 sphericalToWorld(vec2 sphCoord, float r)
{
  return vec3(
      r * sin(sphCoord.y) * cos(sphCoord.x),
      r * sin(sphCoord.y) * sin(sphCoord.x),
      r * cos(sphCoord.y));
}

vec2 worldToSpherical(vec3 flatCoord, float r)
{
  return vec2(
      atan(flatCoord.x, flatCoord.y),
      acos(flatCoord.z / r));
}


mat3 makeRotationMatrix(vec3 a)
{
  return mat3(
      cos(a.x) * cos(a.z) - sin(a.x) * cos(a.y) * sin(a.z),
      -cos(a.x) * sin(a.z) - sin(a.x) * cos(a.y) * cos(a.z),
      sin(a.x) * sin(a.y),
      sin(a.x) * cos(a.z) + cos(a.x) * cos(a.y) * sin(a.z),
      -sin(a.x) * sin(a.z) + cos(a.x) * cos(a.y) * cos(a.z),
      -cos(a.x) * sin(a.y),
      sin(a.y) * sin(a.z),
      sin(a.y) * cos(a.z),
      cos(a.y));
}

void main()
{
  float rotatex = (uOffsets.x+uSetScalar0) * pi * 2.0;
  float rot = (uOffsets.y+uSetScalar1) * pi * 2.0;
  float rotz = (uOffsets.z+uSetScalar2) * pi * 2.0;
  mat3 rota = makeRotationMatrix(vec3(rotatex, rot, rotz));
  vec2 p = vec2(v_TexCoordinate.x, v_TexCoordinate.y) * vec2(2.0 * pi, 1.0 * pi) - vec2(0.5, 0.0) * pi;
  vec3 worldSphCoord = sphericalToWorld(p, 1.0);
  vec3 rotatedWorldSphCoord = normalize(rota * worldSphCoord);
  vec2 rotatedSphericalCoord = worldToSpherical(rotatedWorldSphCoord, 1.0);
  vec2 coords = (rotatedSphericalCoord + vec2(0.5, 0.0) * pi) * vec2(1.0 / (2.0 * pi), 1.0 / (1.0 * pi));
  vec2 croppedCoords = vec2(1.0-coords.x,coords.y);
  outputColor = texture2D(uTexture0, croppedCoords);
}