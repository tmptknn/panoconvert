#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
#include "convertwithgl.hpp"
    /*
    public:
    GLuint vbuffer;
    GLuint tbuffer;
    GLuint TextureID;
    GLuint vertexID;
    GLuint coordID;
    GLuint previewTextureID;
    GLuint previewVertexID;
    GLuint previewCoordID;
    GLuint uSizeID;
    GLuint uOffsets;
    GLuint vao;
    GLuint offScreenTexture;
    GLuint offScreenFrameBuffer;
    GLuint offScreenRenderBuffer;
    */

    GLuint LoadShaders(const char *vertex_file_path, const char *fragment_file_path)
    {

        // Create the shaders
        GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        // Read the Vertex Shader code from the file
        std::string VertexShaderCode;
        std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
        if (VertexShaderStream.is_open())
        {
            std::stringstream sstr;
            sstr << VertexShaderStream.rdbuf();
            VertexShaderCode = sstr.str();
            VertexShaderStream.close();
        }
        else
        {
            printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
            getchar();
            return 0;
        }

        // Read the Fragment Shader code from the file
        std::string FragmentShaderCode;
        std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
        if (FragmentShaderStream.is_open())
        {
            std::stringstream sstr;
            sstr << FragmentShaderStream.rdbuf();
            FragmentShaderCode = sstr.str();
            FragmentShaderStream.close();
        }

        GLint Result = GL_FALSE;
        int InfoLogLength;

        // Compile Vertex Shader
        printf("Compiling shader : %s\n", vertex_file_path);
        char const *VertexSourcePointer = VertexShaderCode.c_str();
        glShaderSource(VertexShaderID, 1, &VertexSourcePointer, 0);
        glCompileShader(VertexShaderID);

        // Check Vertex Shader
        glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0)
        {
            std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(VertexShaderID, InfoLogLength, 0, &VertexShaderErrorMessage[0]);
            printf("%s\n", &VertexShaderErrorMessage[0]);
        }

        // Compile Fragment Shader
        printf("Compiling shader : %s\n", fragment_file_path);
        char const *FragmentSourcePointer = FragmentShaderCode.c_str();
        glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, 0);
        glCompileShader(FragmentShaderID);

        // Check Fragment Shader
        glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0)
        {
            std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(FragmentShaderID, InfoLogLength, 0, &FragmentShaderErrorMessage[0]);
            printf("%s\n", &FragmentShaderErrorMessage[0]);
        }

        // Link the program
        printf("Linking program\n");
        GLuint ProgramID = glCreateProgram();
        glAttachShader(ProgramID, VertexShaderID);
        glAttachShader(ProgramID, FragmentShaderID);
        glLinkProgram(ProgramID);

        // Check the program
        glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
        glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0)
        {
            std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
            glGetProgramInfoLog(ProgramID, InfoLogLength, 0, &ProgramErrorMessage[0]);
            printf("%s\n", &ProgramErrorMessage[0]);
        }

        glDetachShader(ProgramID, VertexShaderID);
        glDetachShader(ProgramID, FragmentShaderID);

        glDeleteShader(VertexShaderID);
        glDeleteShader(FragmentShaderID);

        return ProgramID;
    }

    GLuint createOffscreenFrameBuffer(int width, int height, GLuint texture)
    {
        GLuint offScreenFrameBuffer;
        glCreateFramebuffers(1, &offScreenFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, offScreenFrameBuffer);
        // offScreenFrameBuffer.width = width;
        // offScreenFrameBuffer.height = height;
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width,
                    height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        return offScreenFrameBuffer;
    }

    GLuint createOffscreenRenderBuffer(int width, int height, GLuint texture){
        GLuint offScreenRenderBuffer;
        glCreateRenderbuffers(1, &offScreenRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, offScreenRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
                            width, height);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                            GL_TEXTURE_2D, texture, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                GL_RENDERBUFFER, offScreenRenderBuffer);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return offScreenRenderBuffer;
        
    }

    GLuint GLWindowContext::createOffScreenTexture(int width, int height)
    {
        GLuint offScreenTexture;
        unsigned char *buffer = new unsigned char[width * height * 4];
        glCreateTextures(GL_TEXTURE_2D, 1, &offScreenTexture);
        glBindTexture(GL_TEXTURE_2D, offScreenTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        offScreenFrameBuffer = createOffscreenFrameBuffer(width, height, offScreenTexture);
        offScreenRenderBuffer = createOffscreenRenderBuffer(width,height, offScreenTexture);
        return offScreenTexture;
    }

    GLuint GLWindowContext::createTexture(unsigned char *data, int width, int height, int outputwidth, int outputheight)
    {
        GLuint textureID;
        glGenTextures(1, &textureID);

        // "Bind" the newly created texture : all future texture functions will modify this texture
        glBindTexture(GL_TEXTURE_2D, textureID);

        // Give the image to OpenGL
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        offScreenTexture = createOffScreenTexture(outputwidth, outputheight);
        return textureID;
    }
    /*
    void GLAPIENTRY
    MessageCallback( GLenum source,
                    GLenum type,
                    GLuint id,
                    GLenum severity,
                    GLsizei length,
                    const GLchar* message,
                    const void* userParam )
    {
    fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
                type, severity, message );
    }*/

    void GLWindowContext::initBuffers()
    {
        static const GLfloat g_vertex_buffer_data[] = {
            -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f};
        static const GLfloat g_texcoordinate_buffer_data[] = {
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f};

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        // This will identify our vertex buffer
        // GLuint vbuffer;
        // Generate 1 buffer, put the resulting identifier in vertexbuffer
        glGenBuffers(1, &vbuffer);
        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
        // Give our vertices to OpenGL.
        glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

        // This will identify our vertex buffer
        // GLuint tbuffer;
        // Generate 1 buffer, put the resulting identifier in vertexbuffer
        glGenBuffers(1, &tbuffer);
        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, tbuffer);
        // Give our vertices to OpenGL.
        glBufferData(GL_ARRAY_BUFFER, sizeof(g_texcoordinate_buffer_data), g_texcoordinate_buffer_data, GL_STATIC_DRAW);
    }

    void GLWindowContext::renderPreview(GLuint texture, GLuint previewProgram)
    {
        glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(previewProgram);
        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        // Set our "myTextureSampler" sampler to use Texture Unit 0
        glUniform1i(previewTextureID, 0);
        glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
        glEnableVertexAttribArray(previewVertexID);

        glVertexAttribPointer(
            previewVertexID, // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,               // size
            GL_FLOAT,        // type
            GL_FALSE,        // normalized?
            0,               // stride
            0                // array buffer offset
        );
        glBindBuffer(GL_ARRAY_BUFFER, tbuffer);
        glEnableVertexAttribArray(previewCoordID);

        glVertexAttribPointer(
            previewCoordID, // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,              // size
            GL_FLOAT,       // type
            GL_FALSE,       // normalized?
            0,              // stride
            0               // array buffer offset
        );
        glDrawArrays(GL_TRIANGLES, 0, 6); // Starting from vertex 0; 3 vertices total -> 1 triangle
        glDisableVertexAttribArray(previewVertexID);
        glDisableVertexAttribArray(previewCoordID);
        glBindTexture(GL_TEXTURE_2D, 0);
        // glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void GLWindowContext::render(GLuint texture, GLuint program, int width, int height, 
        float xoffset, float yoffset, float zoffset, float woffset,
        float lLensRot, float rLensRot)
    {
        glClearColor(0.0f, 0.2f, 0.0f, 0.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(program);
        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        // Set our "myTextureSampler" sampler to use Texture Unit 0
        glUniform4f(uOffsets,xoffset,yoffset,zoffset,woffset);
        glUniform2f(uLensRotation,lLensRot,rLensRot);
        glUniform1i(TextureID, 0);
        glUniform2f(uSizeID, (float)width, (float)height);
        glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
        glEnableVertexAttribArray(vertexID);

        glVertexAttribPointer(
            vertexID, // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,        // size
            GL_FLOAT, // type
            GL_FALSE, // normalized?
            0,        // stride
            0         // array buffer offset
        );
        glBindBuffer(GL_ARRAY_BUFFER, tbuffer);
        glEnableVertexAttribArray(coordID);

        glVertexAttribPointer(
            coordID,  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,        // size
            GL_FLOAT, // type
            GL_FALSE, // normalized?
            0,        // stride
            0         // array buffer offset
        );
        glDrawArrays(GL_TRIANGLES, 0, 6); // Starting from vertex 0; 3 vertices total -> 1 triangle
        glDisableVertexAttribArray(vertexID);
        glDisableVertexAttribArray(coordID);
        glBindTexture(GL_TEXTURE_2D, 0);
        // glBindRenderbuffer(GL_RENDERBUFFER, 0);
        // glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }


    void GLWindowContext::drawOriginalData(GLuint texture, GLuint program, int width, int height)
    {
        glViewport(0, 0, width, height);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);//offScreenFrameBuffer);
        renderPreview(texture, program);
    }

    void GLWindowContext::drawData(GLuint texture, GLuint program, int width, int height, 
        float xoffset, float yoffset, float zoffset, float woffset,
        float lLensRot, float rLensRot)
    {
        glViewport(0, 0, width, height);
        glBindFramebuffer(GL_FRAMEBUFFER, offScreenFrameBuffer);
        render(texture, program, width, height, xoffset, yoffset, zoffset, woffset, lLensRot, rLensRot);
    }

    void GLWindowContext::previewData(GLuint previewProgram, int width, int height)
    {
        glViewport(0, 0, width, height);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
        renderPreview(offScreenTexture, previewProgram);
    }

    void _check_gl_error(const char *file, int line)
    {
        GLenum err(glGetError());

        while (err != GL_NO_ERROR)
        {
            string error;

            switch (err)
            {
            case GL_INVALID_OPERATION:
                error = "INVALID_OPERATION";
                break;
            case GL_INVALID_ENUM:
                error = "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "INVALID_VALUE";
                break;
            case GL_OUT_OF_MEMORY:
                error = "OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "INVALID_FRAMEBUFFER_OPERATION";
                break;
            }

            cerr << "GL_" << error.c_str() << " - " << file << ":" << line << endl;
            err = glGetError();
        }
    }

    void GLWindowContext::setupGL(){
        initBuffers();
    }

    GLuint GLWindowContext::loadProgram(std::string path, std::string shadername )
    {
        // During init, enable debug output
        // glEnable              ( GL_DEBUG_OUTPUT );
        // glDebugMessageCallback( MessageCallback, 0 );
        GLuint program = LoadShaders((path+"/shaders/vertexshader.glsl").c_str(), (path+"/shaders/"+shadername).c_str());
        uOffsets = glGetUniformLocation(program, "uOffsets");
        uLensRotation = glGetUniformLocation(program, "uLensRotation");
        TextureID = glGetUniformLocation(program, "uTexture0");
        vertexID = glGetAttribLocation(program, "vertexPosition");
        coordID = glGetAttribLocation(program, "vertexUV");
        uSizeID = glGetUniformLocation(program, "uSize");

        
        return program;
    }

    GLuint GLWindowContext::setupPreviewProgram(std::string path)
    {
        GLuint previewProgram = LoadShaders(
            (path+"/shaders/previewvertexshader.glsl").c_str(), 
            (path+"/shaders/previewfragmentshader.glsl").c_str()
            );
        previewTextureID = glGetUniformLocation(previewProgram, "uTexture0");
        previewVertexID = glGetAttribLocation(previewProgram, "vertexPosition");
        previewCoordID = glGetAttribLocation(previewProgram, "vertexUV");
        return previewProgram;
    }

    void GLWindowContext::addData(unsigned char const **frame, int width, int height, GLuint texture)
    {
        /*
        int moffset =0;
        for(unsigned long int i=0; i< (unsigned long int)height; i++){
            uint8_t* offset =pFrame->data[0]+i*pFrame->linesize[0];
            memcpy(&data[moffset],offset,width*3);
            moffset+=width*3;
        }*/

        // memcpy(data,(uint8_t const * const *)(pFrame->data[0]),width*height*3);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, frame);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    GLFWwindow* GLWindowContext::init(int w, int h, std::string window_name)
    {
        // glewExperimental = GL_TRUE;
        //  start GL context and O/S window using the GLFW helper library
        GLFWwindow *window;
        // uncomment these lines if on Apple OS X
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        // glfwWindowHint(GLFW_DECORATED, GL_FALSE);

        window = glfwCreateWindow(w, h, window_name.c_str(), 0, 0);
        if (!window)
        {
            fprintf(stderr, "ERROR: could not open window with GLFW3\n");
            glfwTerminate();
            return window;
        }
        glfwMakeContextCurrent(window);

        glewExperimental = true;
        GLenum err = glewInit();
        if (err != GLEW_OK)
        {
            // Problem: glewInit failed, something is seriously wrong.
            cout << "glewInit failed: " << glewGetErrorString(err) << endl;
            exit(1);
        }
        // start GLEW extension handler
        // glewExperimental = GL_TRUE;
        // glewInit();
        // get version info

        const GLubyte *renderer = glGetString(GL_RENDERER); // get renderer string
        const GLubyte *version = glGetString(GL_VERSION);   // version as a string
        printf("Renderer: %s\n", renderer);
        printf("OpenGL version supported %s\n", version);

        // tell GL to only draw onto a pixel if the shape is closer to the viewer
        //glEnable(GL_DEPTH_TEST); // enable depth-testing
        //glDepthFunc(GL_LESS);    // depth-testing interprets a smaller value as "closer"
        //glViewport(0, 0, w, h);
        //glOrtho(-1.0, 1.0, -1, 0, 1.0, 100.0);
        return window;
    }

    void GLWindowContext::readInFrame(unsigned char const **frame, int width, int height)
    {
        /*
        int moffset =0;
        for(unsigned long int i=0; i< (unsigned long int)height; i++){
            uint8_t* offset =pFrame->data[0]+i*pFrame->linesize[0];
            memcpy(&data[moffset],offset,width*3);
            moffset+=width*3;
        }*/
        // glViewport      (0, 0, (GLint)width, (GLint)height);

        glBindFramebuffer(GL_FRAMEBUFFER, offScreenFrameBuffer);
        // glPixelStorei(GL_PACK_ALIGNMENT,1);
        glBindTexture(GL_TEXTURE_2D, offScreenTexture);
        glReadBuffer(GL_COLOR_ATTACHMENT0);
        glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, frame);
        // memcpy((uint8_t const * *)(pFrame->data[0]),(uint8_t *)data,(size_t)(width*height*3));
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
