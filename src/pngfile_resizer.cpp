#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <string>
#include <limits.h>
#include <png.h>


png_byte color_type;
png_byte bit_depth;
png_bytep *row_pointers = NULL;
png_bytep *row_pointers2 = NULL;

/*
void process_png_file(IMAGE_INFO* image_info)
{
    for (int y = 0; y < image_info->height; y++)
    {
        png_bytep row = row_pointers[y];

        ///*
        for (int x = 0; x < image_info->width; x++)
        {
            png_bytep px = &(row[x * 4]);

            int offset = image_info->width * 3 * sizeof(unsigned char) * y + 3 * sizeof(unsigned char) * x;
            memcpy(&image_info->data[offset], px, 3 * sizeof(unsigned char));
        }
    }
}
*/

void process_lines(png_structp png, png_structp png_out, int width, int height, png_bytep *row_pointers, png_bytep *row_pointers2){
    for (int y = 0; y < height; y++)
    {
        png_bytep row = row_pointers[y];
        png_bytep row_out = row_pointers2[y/2];
        ///*
        for (int x = 0; x < width; x++)
        {
            if((y%2 == 0) && (x%2 == 0)){
                png_bytep px = &(row[x * 4]);
                png_bytep px_out = &(row_out[(x/2)*4]);
                memcpy(px_out, px, 3 * sizeof(unsigned char));
                px_out[3] =255;
            }
        }
    }

}

void half_png_file(const char *filename, const char *outputfilename)
{

    if (row_pointers){
        free(row_pointers);
        row_pointers=NULL;
    }
    FILE *fp = fopen(filename, "rb");

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png)
        abort();

    png_infop info = png_create_info_struct(png);
    if (!info)
        abort();

    if (setjmp(png_jmpbuf(png)))
        abort();

    png_init_io(png, fp);

    png_read_info(png, info);

    //png_image_info.width = png_get_image_width(png, info);
    //png_image_info.height = png_get_image_height(png, info);

    int width = png_get_image_width(png, info);
    int height = png_get_image_height(png, info);
    color_type = png_get_color_type(png, info);
    bit_depth = png_get_bit_depth(png, info);

    // Read any color_type into 8bit depth, RGBA format.
    // See http://www.libpng.org/pub/png/libpng-manual.txt

    if (bit_depth == 16)
        png_set_strip_16(png);

    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png);

    // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_expand_gray_1_2_4_to_8(png);

    if (png_get_valid(png, info, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha(png);

    // These color_type don't have an alpha channel then fill it with 0xff.
    if (color_type == PNG_COLOR_TYPE_RGB ||
        color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

    if (color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png);

    png_read_update_info(png, info);

    row_pointers = (png_bytep *)malloc(sizeof(png_bytep) * height);
    for (int y = 0; y < height; y++)
    {
        row_pointers[y] = (png_byte *)malloc(png_get_rowbytes(png, info));
    }



    png_read_image(png, row_pointers);

    png_structp png_out = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_out)
        abort();

    png_infop info_out = png_create_info_struct(png_out);
    if (!info)
        abort();



    if (setjmp(png_jmpbuf(png_out)))
        abort();
    FILE *fp_out = fopen(outputfilename, "wb");
    if (!fp_out)
        abort();
    png_init_io(png_out, fp_out);

    // Output is 8bit depth, RGBA format.
    png_set_IHDR(
        png_out,
        info_out,
        width/2, height/2,
        8,
        PNG_COLOR_TYPE_RGBA,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_out, info_out);  

    row_pointers2 = (png_bytep *)malloc(sizeof(png_bytep) * height/2);
    for (int y = 0; y < height/2; y++)
    {
        row_pointers2[y] = (png_byte *)malloc(png_get_rowbytes(png_out, info_out));
    }

    

    process_lines(png, png_out, width,height,row_pointers, row_pointers2);
    png_write_image(png_out, row_pointers2);
    png_write_end(png_out, NULL);
    fclose(fp);
    fclose(fp_out);
    png_destroy_read_struct(&png, &info, NULL);
    
    // Could check if this allocated???
    //png_image_info.data = new unsigned char[png_image_info.width*png_image_info.height*4];
    //process_png_file(&png_image_info);
    
    //return png_image_info;
}
/*
void write_png_data(IMAGE_INFO info)
{
    for (int y = 0; y < info.height; y++)
    {
        png_bytep row = row_pointers2[y];

        ///*
        for (int x = 0; x < info.width; x++)
        {
            png_bytep px = &(row[x * 4]);

            int offset = info.width * 3 * sizeof(unsigned char) * y + 3 * sizeof(unsigned char) * x;
            memcpy(px, &info.data[offset], 3 * sizeof(unsigned char));
            px[3] = 255;
        }
        //*/
  //  }
//}
//*/
/*
void write_png_file(const char *filename, IMAGE_INFO image_info)
{

    FILE *fp = fopen(filename, "wb");
    if (!fp)
        abort();

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png)
        abort();

    png_infop info = png_create_info_struct(png);
    if (!info)
        abort();



    if (setjmp(png_jmpbuf(png)))
        abort();

    png_init_io(png, fp);

    // Output is 8bit depth, RGBA format.
    png_set_IHDR(
        png,
        info,
        image_info.width, image_info.height,
        8,
        PNG_COLOR_TYPE_RGBA,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png, info);

    row_pointers2 = (png_bytep *)malloc(sizeof(png_bytep) * image_info.height);
    for (int y = 0; y < image_info.height; y++)
    {
        row_pointers2[y] = (png_byte *)malloc(png_get_rowbytes(png, info));
    }

    write_png_data(image_info);
    // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
    // Use png_set_filler().
    // png_set_filler(png, 0, PNG_FILLER_AFTER);

    if (!row_pointers2)
        abort();

    png_write_image(png, row_pointers2);
    png_write_end(png, NULL);

    for (int y = 0; y < image_info.height; y++)
    {
        free(row_pointers2[y]);
    }
    free(row_pointers2);
    row_pointers2 = nullptr;
    fclose(fp);

    png_destroy_write_struct(&png, &info);
}
*/
int main(void){

    half_png_file("testhuge.png","testhalf.png");
}