#include "pngconverter.h"
#include "parameter_parser.hpp"
#include "exifinjector.hpp"
#include <iostream>


int main(int argc, char *argv[])
{
    ParameterParser pp;
    if(!pp.parseParameters(argc,argv)) return 0;
    setProgramSourceType(pp.inputType);
    start(argc, argv, pp.ifile);
    while (running())
    {       
        idleFunc();
    }

    save( pp.ofile, pp.ifile);
    quit();
}