/**
 * @file
 * API example for demuxing, decoding, filtering, encoding and muxing
 * @example transcoding.c
 */
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;
#include <stdio.h>
#include <stdlib.h>
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
// #include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
}
#include <GLFW/glfw3.h>
#include <GL/glu.h>
#include "convertwithgl.hpp"
#include "videoconverter.h"

GLFWwindow *window;
GLWindowContext glwindow;
int outputwidth = 3840;
int outputheight = 1920;

static AVFormatContext *ifmt_ctx;
static AVFormatContext *ofmt_ctx;
typedef struct FilteringContext
{
  AVFilterContext *buffersink_ctx;
  AVFilterContext *buffersrc_ctx;
  AVFilterGraph *filter_graph;
} FilteringContext;
static FilteringContext *filter_ctx;
typedef struct StreamContext
{
  AVCodecContext *dec_ctx;
  AVCodecContext *enc_ctx;
} StreamContext;
static StreamContext *stream_ctx;

void readFrame(AVFrame *pFrame, int width, int height)
{

  glwindow.readInFrame((uint8_t const **)(pFrame->data[0]), width, height);
  /*
    int moffset =0;
    for(unsigned long int i=0; i< (unsigned long int)height; i++){
      uint8_t* offset =pFrame->data[0]+i*pFrame->linesize[0];
      memcpy(&data[moffset],offset,width*3);
      moffset+=width*3;
    }*/
  /*
// glViewport      (0, 0, (GLint)width, (GLint)height);
glBindBuffer(GL_FRAMEBUFFER, offScreenFrameBuffer);
// glPixelStorei(GL_PACK_ALIGNMENT,1);
glBindTexture(GL_TEXTURE_2D, offScreenTexture);
glReadBuffer(GL_COLOR_ATTACHMENT0);
glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, (uint8_t const **)(pFrame->data[0]));

// memcpy((uint8_t const * *)(pFrame->data[0]),(uint8_t *)data,(size_t)(width*height*3));

glBindBuffer(GL_FRAMEBUFFER, NULL);
glBindTexture(GL_TEXTURE_2D, NULL);
// check_gl_error();
*/
}

static void error_callback(int error, const char *description)
{
  fprintf(stderr, "Error%i: %s\n", error, description);
}

static int open_input_file(const char *filename)
{
  int ret;
  unsigned int i;
  ifmt_ctx = NULL;
  if ((ret = avformat_open_input(&ifmt_ctx, filename, NULL, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
    return ret;
  }
  if ((ret = avformat_find_stream_info(ifmt_ctx, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
    return ret;
  }
  stream_ctx = (StreamContext *)av_mallocz_array(ifmt_ctx->nb_streams, sizeof(*stream_ctx));
  if (!stream_ctx)
    return AVERROR(ENOMEM);
  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    AVStream *stream = ifmt_ctx->streams[i];
    AVCodec *dec = avcodec_find_decoder(stream->codecpar->codec_id);
    AVCodecContext *codec_ctx;
    if (!dec)
    {
      av_log(NULL, AV_LOG_ERROR, "Failed to find decoder for stream #%u\n", i);
      return AVERROR_DECODER_NOT_FOUND;
    }
    codec_ctx = avcodec_alloc_context3(dec);
    if (!codec_ctx)
    {
      av_log(NULL, AV_LOG_ERROR, "Failed to allocate the decoder context for stream #%u\n", i);
      return AVERROR(ENOMEM);
    }
    ret = avcodec_parameters_to_context(codec_ctx, stream->codecpar);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Failed to copy decoder parameters to input decoder context "
                                 "for stream #%u\n",
             i);
      return ret;
    }
    /* Reencode video & audio and remux subtitles etc. */
    if (codec_ctx->codec_type == AVMEDIA_TYPE_VIDEO || codec_ctx->codec_type == AVMEDIA_TYPE_AUDIO)
    {
      if (codec_ctx->codec_type == AVMEDIA_TYPE_VIDEO)
        codec_ctx->framerate = av_guess_frame_rate(ifmt_ctx, stream, NULL);
      /* Open decoder */
      ret = avcodec_open2(codec_ctx, dec, NULL);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Failed to open decoder for stream #%u\n", i);
        return ret;
      }
    }
    stream_ctx[i].dec_ctx = codec_ctx;
  }
  av_dump_format(ifmt_ctx, 0, filename, 0);
  return 0;
}
static int open_output_file(const char *filename)
{
  AVStream *out_stream;
  AVStream *in_stream;
  AVCodecContext *dec_ctx, *enc_ctx;
  AVCodec *encoder;
  int ret;
  unsigned int i;
  ofmt_ctx = NULL;
  avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, filename);
  if (!ofmt_ctx)
  {
    av_log(NULL, AV_LOG_ERROR, "Could not create output context\n");
    return AVERROR_UNKNOWN;
  }
  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream)
    {
      av_log(NULL, AV_LOG_ERROR, "Failed allocating output stream\n");
      return AVERROR_UNKNOWN;
    }
    in_stream = ifmt_ctx->streams[i];
    dec_ctx = stream_ctx[i].dec_ctx;
    if (dec_ctx->codec_type == AVMEDIA_TYPE_VIDEO || dec_ctx->codec_type == AVMEDIA_TYPE_AUDIO)
    {
      /* in this example, we choose transcoding to same codec */
      encoder = avcodec_find_encoder(dec_ctx->codec_id);
      if (!encoder)
      {
        av_log(NULL, AV_LOG_FATAL, "Necessary encoder not found\n");
        return AVERROR_INVALIDDATA;
      }
      enc_ctx = avcodec_alloc_context3(encoder);
      if (!enc_ctx)
      {
        av_log(NULL, AV_LOG_FATAL, "Failed to allocate the encoder context\n");
        return AVERROR(ENOMEM);
      }
      /* In this example, we transcode to same properties (picture size,
       * sample rate etc.). These properties can be changed for output
       * streams easily using filters */
      if (dec_ctx->codec_type == AVMEDIA_TYPE_VIDEO)
      {
        enc_ctx->height = outputheight;
        enc_ctx->width = outputwidth;
        enc_ctx->sample_aspect_ratio.num = dec_ctx->sample_aspect_ratio.num * 2;
        enc_ctx->sample_aspect_ratio.den = dec_ctx->sample_aspect_ratio.den;
        /* take first format from list of supported formats */
        if (encoder->pix_fmts)
          enc_ctx->pix_fmt = encoder->pix_fmts[0];
        else
          enc_ctx->pix_fmt = dec_ctx->pix_fmt;
        /* video time_base can be set to whatever is handy and supported by encoder */
        enc_ctx->time_base = av_inv_q(dec_ctx->framerate);
        enc_ctx->time_base.den = av_inv_q(dec_ctx->framerate).den * 2;
      }
      else
      {
        enc_ctx->sample_rate = dec_ctx->sample_rate;
        enc_ctx->channel_layout = dec_ctx->channel_layout;
        enc_ctx->channels = av_get_channel_layout_nb_channels(enc_ctx->channel_layout);
        /* take first format from list of supported formats */
        enc_ctx->sample_fmt = encoder->sample_fmts[0];
        enc_ctx->time_base = (AVRational){1, enc_ctx->sample_rate};
      }
      /* Third parameter can be used to pass settings to encoder */
      ret = avcodec_open2(enc_ctx, encoder, NULL);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video encoder for stream #%u\n", i);
        return ret;
      }
      ret = avcodec_parameters_from_context(out_stream->codecpar, enc_ctx);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Failed to copy encoder parameters to output stream #%u\n", i);
        return ret;
      }
      if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
      out_stream->time_base = enc_ctx->time_base;
      stream_ctx[i].enc_ctx = enc_ctx;
    }
    else if (dec_ctx->codec_type == AVMEDIA_TYPE_UNKNOWN)
    {
      av_log(NULL, AV_LOG_FATAL, "Elementary stream #%d is of unknown type, cannot proceed\n", i);
      return AVERROR_INVALIDDATA;
    }
    else
    {
      /* if this stream must be remuxed */
      ret = avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Copying parameters for stream #%u failed\n", i);
        return ret;
      }
      out_stream->time_base = in_stream->time_base;
    }
  }
  av_dump_format(ofmt_ctx, 0, filename, 1);
  if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
  {
    ret = avio_open(&ofmt_ctx->pb, filename, AVIO_FLAG_WRITE);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Could not open output file '%s'", filename);
      return ret;
    }
  }
  /* init muxer, write output file header */
  ret = avformat_write_header(ofmt_ctx, NULL);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Error occurred when opening output file\n");
    return ret;
  }
  return 0;
}
static int init_filter(FilteringContext *fctx, AVCodecContext *dec_ctx,
                       AVCodecContext *enc_ctx, const char *filter_spec)
{
  char args[512];
  int ret = 0;
  const AVFilter *buffersrc = NULL;
  const AVFilter *buffersink = NULL;
  AVFilterContext *buffersrc_ctx = NULL;
  AVFilterContext *buffersink_ctx = NULL;
  AVFilterInOut *outputs = avfilter_inout_alloc();
  AVFilterInOut *inputs = avfilter_inout_alloc();
  AVFilterGraph *filter_graph = avfilter_graph_alloc();
  if (!outputs || !inputs || !filter_graph)
  {
    ret = AVERROR(ENOMEM);
    goto end;
  }
  if (dec_ctx->codec_type == AVMEDIA_TYPE_VIDEO)
  {
    buffersrc = avfilter_get_by_name("buffer");
    buffersink = avfilter_get_by_name("buffersink");
    if (!buffersrc || !buffersink)
    {
      av_log(NULL, AV_LOG_ERROR, "filtering source or sink element not found\n");
      ret = AVERROR_UNKNOWN;
      goto end;
    }
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt,
             dec_ctx->time_base.num, dec_ctx->time_base.den,
             dec_ctx->sample_aspect_ratio.num,
             dec_ctx->sample_aspect_ratio.den);
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot create buffer source\n");
      goto end;
    }
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot create buffer sink\n");
      goto end;
    }
    ret = av_opt_set_bin(buffersink_ctx, "pix_fmts",
                         (uint8_t *)&enc_ctx->pix_fmt, sizeof(enc_ctx->pix_fmt),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot set output pixel format\n");
      goto end;
    }
  }
  else if (dec_ctx->codec_type == AVMEDIA_TYPE_AUDIO)
  {
    buffersrc = avfilter_get_by_name("abuffer");
    buffersink = avfilter_get_by_name("abuffersink");
    if (!buffersrc || !buffersink)
    {
      av_log(NULL, AV_LOG_ERROR, "filtering source or sink element not found\n");
      ret = AVERROR_UNKNOWN;
      goto end;
    }
    if (!dec_ctx->channel_layout)
      dec_ctx->channel_layout =
          av_get_default_channel_layout(dec_ctx->channels);
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIx64,
             dec_ctx->time_base.num, dec_ctx->time_base.den, dec_ctx->sample_rate,
             av_get_sample_fmt_name(dec_ctx->sample_fmt),
             dec_ctx->channel_layout);
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot create audio buffer source\n");
      goto end;
    }
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot create audio buffer sink\n");
      goto end;
    }
    ret = av_opt_set_bin(buffersink_ctx, "sample_fmts",
                         (uint8_t *)&enc_ctx->sample_fmt, sizeof(enc_ctx->sample_fmt),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot set output sample format\n");
      goto end;
    }
    ret = av_opt_set_bin(buffersink_ctx, "channel_layouts",
                         (uint8_t *)&enc_ctx->channel_layout,
                         sizeof(enc_ctx->channel_layout), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot set output channel layout\n");
      goto end;
    }
    ret = av_opt_set_bin(buffersink_ctx, "sample_rates",
                         (uint8_t *)&enc_ctx->sample_rate, sizeof(enc_ctx->sample_rate),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Cannot set output sample rate\n");
      goto end;
    }
  }
  else
  {
    ret = AVERROR_UNKNOWN;
    goto end;
  }
  /* Endpoints for the filter graph. */
  outputs->name = av_strdup("in");
  outputs->filter_ctx = buffersrc_ctx;
  outputs->pad_idx = 0;
  outputs->next = NULL;
  inputs->name = av_strdup("out");
  inputs->filter_ctx = buffersink_ctx;
  inputs->pad_idx = 0;
  inputs->next = NULL;
  if (!outputs->name || !inputs->name)
  {
    ret = AVERROR(ENOMEM);
    goto end;
  }
  if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                      &inputs, &outputs, NULL)) < 0)
    goto end;
  if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    goto end;
  /* Fill FilteringContext */
  fctx->buffersrc_ctx = buffersrc_ctx;
  fctx->buffersink_ctx = buffersink_ctx;
  fctx->filter_graph = filter_graph;
end:
  avfilter_inout_free(&inputs);
  avfilter_inout_free(&outputs);
  return ret;
}
static int init_filters(void)
{
  const char *filter_spec;
  unsigned int i;
  int ret;
  filter_ctx = (FilteringContext *)av_malloc_array(ifmt_ctx->nb_streams, sizeof(*filter_ctx));
  if (!filter_ctx)
    return AVERROR(ENOMEM);
  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    filter_ctx[i].buffersrc_ctx = NULL;
    filter_ctx[i].buffersink_ctx = NULL;
    filter_ctx[i].filter_graph = NULL;
    if (!(ifmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO || ifmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO))
      continue;
    if (ifmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
      filter_spec = "null"; /* passthrough (dummy) filter for video */
    else
      filter_spec = "anull"; /* passthrough (dummy) filter for audio */
    ret = init_filter(&filter_ctx[i], stream_ctx[i].dec_ctx,
                      stream_ctx[i].enc_ctx, filter_spec);
    if (ret)
      return ret;
  }
  return 0;
}
static int encode_write_frame(AVFrame *filt_frame, unsigned int stream_index, int *got_frame)
{
  int ret;
  int got_frame_local;
  AVPacket enc_pkt;
  int (*enc_func)(AVCodecContext *, AVPacket *, const AVFrame *, int *) =
      (ifmt_ctx->streams[stream_index]->codecpar->codec_type ==
       AVMEDIA_TYPE_VIDEO)
          ? avcodec_encode_video2
          : avcodec_encode_audio2;
  if (!got_frame)
    got_frame = &got_frame_local;
  av_log(NULL, AV_LOG_INFO, "Encoding frame\n");
  /* encode filtered frame */
  enc_pkt.data = NULL;
  enc_pkt.size = 0;
  av_init_packet(&enc_pkt);
  ret = enc_func(stream_ctx[stream_index].enc_ctx, &enc_pkt,
                 filt_frame, got_frame);
  av_frame_free(&filt_frame);
  if (ret < 0)
    return ret;
  if (!(*got_frame))
    return 0;
  /* prepare packet for muf.xing */
  enc_pkt.stream_index = stream_index;
  av_packet_rescale_ts(&enc_pkt,
                       stream_ctx[stream_index].enc_ctx->time_base,
                       ofmt_ctx->streams[stream_index]->time_base);
  av_log(NULL, AV_LOG_DEBUG, "Muxing frame\n");
  /* mux encoded frame */
  ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
  return ret;
}
static int filter_encode_write_frame(AVFrame *frame, unsigned int stream_index)
{
  int ret;
  AVFrame *filt_frame;
  av_log(NULL, AV_LOG_INFO, "Pushing decoded frame to filters\n");
  /* push the decoded frame into the filtergraph */
  ret = av_buffersrc_add_frame_flags(filter_ctx[stream_index].buffersrc_ctx,
                                     frame, 0);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
    return ret;
  }
  /* pull filtered frames from the filtergraph */
  while (1)
  {
    filt_frame = av_frame_alloc();
    if (!filt_frame)
    {
      av_log(NULL, AV_LOG_ERROR, "Error in alloc");
      ret = AVERROR(ENOMEM);
      break;
    }
    av_log(NULL, AV_LOG_INFO, "Pulling filtered frame from filters\n");
    ret = av_buffersink_get_frame(filter_ctx[stream_index].buffersink_ctx,
                                  filt_frame);
    if (ret < 0)
    {
      /* if no more frames for output - returns AVERROR(EAGAIN)
       * if flushed and no more frames for output - returns AVERROR_EOF
       * rewrite retcode to 0 to show it as normal procedure completion
       */
      if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        ret = 0;
      av_frame_free(&filt_frame);
      break;
    }
    filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
    ret = encode_write_frame(filt_frame, stream_index, NULL);
    if (ret < 0)
      break;
  }
  return ret;
}
static int flush_encoder(unsigned int stream_index)
{
  int ret;
  int got_frame;
  if (!(stream_ctx[stream_index].enc_ctx->codec->capabilities &
        AV_CODEC_CAP_DELAY))
    return 0;
  while (1)
  {
    av_log(NULL, AV_LOG_INFO, "Flushing stream #%u encoder\n", stream_index);
    ret = encode_write_frame(NULL, stream_index, &got_frame);
    if (ret < 0)
      break;
    if (!got_frame)
      return 0;
  }
  return ret;
}
int main(int argc, char **argv)
{
  int ret;
  AVPacket packet; // = new AVPacket();
  packet.data = NULL;
  packet.size = 0;
  AVFrame *frame = NULL;
  enum AVMediaType type;
  unsigned int stream_index;
  unsigned int i;
  int got_frame;
  int (*dec_func)(AVCodecContext *, AVFrame *, int *, const AVPacket *);
  unsigned char *data;
  GLuint texture = 0;
  GLuint program = 0;
  GLuint previewProgram = 0;
  int numBytes = 0;
  int numBytes2 = 0;
  int inputwidth = 3840;
  int inputheight = 1920;

  int windowwidth = 800;
  int windowheight = 400;
  int frameIndex = 0;
  AVFrame *pFrameRGB = NULL;

  uint8_t *buffer = NULL;
  struct SwsContext *sws_ctx = NULL;
  AVFrame *pFrameRGBout = NULL;
  uint8_t *buffer2 = NULL;
  struct SwsContext *sws_ctx2 = NULL;
  AVFrame *frame2 = NULL;
  uint8_t *frame2buffer = NULL;
  int numBytesFrame2 = 0;

  char buf[PATH_MAX];
  char *res = realpath(argv[0], buf);
  (void)argc;                      /* make compiler happy */
  std::string whole_path =  std::string(buf);
  std::string path = whole_path.substr(0,whole_path.find_last_of('/'));
    

  if (argc != 3)
  {
    av_log(NULL, AV_LOG_ERROR, "Usage: %s <input file> <output file>\n", argv[0]);
    return 1;
  }

  glfwSetErrorCallback(error_callback);
  if (!glfwInit())
  {
    fprintf(stderr, "ERROR: could not start GLFW3\n");
    return 1;
  }

  av_register_all();
  avfilter_register_all();
  if ((ret = open_input_file(argv[1])) < 0)
    goto end;

  inputwidth = stream_ctx->dec_ctx->width;
  inputheight = stream_ctx->dec_ctx->height;
  if ((ret = open_output_file(argv[2])) < 0)
    goto end;
  if ((ret = init_filters()) < 0)
    goto end;

  // Allocate an AVFrame structure
  pFrameRGB = av_frame_alloc();
  if (pFrameRGB == NULL)
    return -1;

  // Determine required buffer size and allocate buffer
  numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, inputwidth,
                                inputheight);

  pFrameRGBout = av_frame_alloc();
  if (pFrameRGBout == NULL)
    return -1;

  // Determine required buffer size and allocate buffer
  numBytes2 = avpicture_get_size(AV_PIX_FMT_RGB24, outputwidth,
                                 outputheight);

  frame2 = av_frame_alloc();
  if (frame2 == NULL)
    return -1;
  numBytesFrame2 = avpicture_get_size(AV_PIX_FMT_YUV420P, outputwidth,
                                      outputheight);

  window = glwindow.init(windowwidth, windowheight);
  if (!window)
    return 1;

  glwindow.setupGL();
  
  program = glwindow.loadProgram(path);
  previewProgram = glwindow.setupPreviewProgram(path);
  data = new unsigned char[inputwidth * inputheight * 3];

  texture = glwindow.createTexture(data, inputwidth, inputheight, outputwidth, outputheight);
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));

  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_RGB24,
                 inputwidth, inputheight);

  // initialize SWS context for software scaling
  sws_ctx = sws_getContext(inputwidth,
                           inputheight,
                           AV_PIX_FMT_YUV420P,
                           inputwidth,
                           inputheight,
                           AV_PIX_FMT_RGB24,
                           SWS_BILINEAR,
                           NULL,
                           NULL,
                           NULL);

  buffer2 = (uint8_t *)av_malloc(numBytes2 * 2 * sizeof(uint8_t));

  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGBout, buffer2, AV_PIX_FMT_RGB24,
                 outputwidth, outputheight);

  // initialize SWS context for software scaling
  sws_ctx2 = sws_getContext(outputwidth,
                            outputheight,
                            AV_PIX_FMT_RGB24,
                            outputwidth,
                            outputheight,
                            AV_PIX_FMT_YUV420P,
                            SWS_BILINEAR,
                            NULL,
                            NULL,
                            NULL);

  frame2buffer = (uint8_t *)av_malloc(numBytesFrame2 * 2 * sizeof(uint8_t));
  // frame2->buf = frame2buffer;
  //  Assign appropriate parts of buffer to image planes in pFrameRGB
  //  Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  //  of AVPicture
  avpicture_fill((AVPicture *)frame2, frame2buffer, AV_PIX_FMT_YUV420P,
                 outputwidth, outputheight);

  /* read all packets */
  while (1)
  {
    if ((ret = av_read_frame(ifmt_ctx, &packet)) < 0)
      break;
    stream_index = packet.stream_index;
    type = ifmt_ctx->streams[packet.stream_index]->codecpar->codec_type;
    av_log(NULL, AV_LOG_DEBUG, "Demuxer gave frame of stream_index %u\n",
           stream_index);
    if (filter_ctx[stream_index].filter_graph)
    {
      av_log(NULL, AV_LOG_DEBUG, "Going to reencode&filter the frame\n");
      frame = av_frame_alloc();
      if (!frame)
      {
        ret = AVERROR(ENOMEM);
        break;
      }
      av_packet_rescale_ts(&packet,
                           ifmt_ctx->streams[stream_index]->time_base,
                           stream_ctx[stream_index].dec_ctx->time_base);
      dec_func = (type == AVMEDIA_TYPE_VIDEO) ? avcodec_decode_video2 : avcodec_decode_audio4;
      ret = dec_func(stream_ctx[stream_index].dec_ctx, frame,
                     &got_frame, &packet);
      if (ret < 0)
      {
        av_frame_free(&frame);
        av_log(NULL, AV_LOG_ERROR, "Decoding failed\n");
        break;
      }
      if (got_frame)
      {
        printf("frame %i %i %i %i %i %i x %i\n", frameIndex, frame->format, frame->linesize[0], frame->width, frame->height, frame->key_frame, frame->nb_samples);
        if (frame->format == AV_PIX_FMT_YUVJ420P)
        {
          int h1 = sws_scale(sws_ctx, (uint8_t const *const *)frame->data,
                             frame->linesize, 0, inputheight,
                             pFrameRGB->data, pFrameRGB->linesize);

          if (h1 == 0)
          {
            printf("Tuhannen pareiksi man %i\n", inputheight);
            // pFrameRGB->height = height;
            // pFrameRGB->width = width;
          }

          glwindow.addData((uint8_t const **)(pFrameRGB->data[0]), inputwidth, inputheight, texture);
          glwindow.drawData(texture, program, outputwidth, outputheight);
          readFrame(pFrameRGBout, outputwidth, outputheight);
          glwindow.previewData(previewProgram, windowwidth, windowheight);

          //}

          glfwSwapBuffers(window);
          int h = sws_scale(sws_ctx2, (uint8_t const *const *)pFrameRGBout->data,
                            pFrameRGBout->linesize, 0, outputheight,
                            frame2->data, frame2->linesize);

          if (h == 0)
          {
            printf("Vituix man \n");
          }

          glfwPollEvents();
          frame2->format = frame->format;
          frame2->width = outputwidth;
          frame2->height = outputheight;
          frame2->key_frame = frame->key_frame;
          frame2->pts = frame->best_effort_timestamp * 2; // av_frame_get_best_effort_timestamp(frame);
          ret = filter_encode_write_frame(frame2, stream_index);
        }
        else
        {
          ret = filter_encode_write_frame(frame, stream_index);
        }
        /*
          frame->pts = frame->best_effort_timestamp;
          ret = filter_encode_write_frame(frame, stream_index);
          */
        av_frame_free(&frame);
        if (ret < 0)
          goto end;
      }
      else
      {
        av_frame_free(&frame);
      }
    }
    else
    {
      /* remux this frame without reencoding */
      av_packet_rescale_ts(&packet,
                           ifmt_ctx->streams[stream_index]->time_base,
                           ofmt_ctx->streams[stream_index]->time_base);
      ret = av_interleaved_write_frame(ofmt_ctx, &packet);
      if (ret < 0)
        goto end;
    }
    av_packet_unref(&packet);
  }
  /* flush filters and encoders */
  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    /* flush filter */
    if (!filter_ctx[i].filter_graph)
      continue;
    ret = filter_encode_write_frame(NULL, i);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Flushing filter failed\n");
      goto end;
    }
    /* flush encoder */
    ret = flush_encoder(i);
    if (ret < 0)
    {
      av_log(NULL, AV_LOG_ERROR, "Flushing encoder failed\n");
      goto end;
    }
  }
  av_write_trailer(ofmt_ctx);
end:
  av_packet_unref(&packet);
  av_frame_free(&frame);
  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    avcodec_free_context(&stream_ctx[i].dec_ctx);
    if (ofmt_ctx && ofmt_ctx->nb_streams > i && ofmt_ctx->streams[i] && stream_ctx[i].enc_ctx)
      avcodec_free_context(&stream_ctx[i].enc_ctx);
    if (filter_ctx && filter_ctx[i].filter_graph)
      avfilter_graph_free(&filter_ctx[i].filter_graph);
  }
  av_free(filter_ctx);
  av_free(stream_ctx);
  avformat_close_input(&ifmt_ctx);
  if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
    avio_closep(&ofmt_ctx->pb);
  avformat_free_context(ofmt_ctx);
  if (ret < 0)
    av_log(NULL, AV_LOG_ERROR, "Error occurred: \n");
  return ret ? 1 : 0;
}

//  g++ -w  videoconverter.cpp -L/usr/lib/x86_64-linux-gnu -lavutil -lavcodec -lswscale -lavformat -lavfilter -lstdc++ -lGLEW -lglfw -lrt -lm -ldl -lXrandr -lXinerama -lXi -lXcursor -lXrender -lGL -lm -lpthread -pthread -ldl -ldrm -lXdamage -lXfixes -lX11-xcb -lxcb-glx -lxcb-dri2 -lXxf86vm -lXext -lX11 -lpthread -lxcb -lXau -lXdmcp -I/usr/includelibdrm -oconv
