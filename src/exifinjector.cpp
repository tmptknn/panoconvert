
#include "exifinjector.hpp"
#include "image_handling.hpp"
#include <stdio.h>



void ExifInjector::readDataFromFile(std::string fileName){
    Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(fileName.c_str());
    assert(image.get() != 0);
    image->readMetadata();
    Exiv2::ExifData readExifData = image->exifData();
    if(!readExifData.empty()){
        exifData = readExifData;
    }
    Exiv2::XmpData readXmpData = image->xmpData();
    if(!readXmpData.empty()){
        xmpData = readXmpData;
    }
    Exiv2::IptcData readIptcData = image->iptcData();
    if(!readIptcData.empty()){
        iptcData = readIptcData;
    }
}

void ExifInjector::writeDataToFile(std::string fileName, IMAGE_INFO imageInfo){
    Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(fileName);
    assert(image.get() != 0);

    if(!exifData.empty()){
        image->setExifData(exifData);
    }
    Exiv2::XmpData::iterator pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.ProjectionType"));
    if (pos == xmpData.end()){

        xmpData["Xmp.GPano.ProjectionType"] = "equirectangular";
    }
    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.UsePanoramaViewer"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.UsePanoramaViewer"] = true;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.FullPanoWidthPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.FullPanoWidthPixels"] = imageInfo.width;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.FullPanoHeightPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.FullPanoHeightPixels"] = imageInfo.height;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.CroppedAreaImageWidthPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.CroppedAreaImageWidthPixels"] = imageInfo.width;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.CroppedAreaImageHeightPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.CroppedAreaImageHeightPixels"] = imageInfo.height;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.CroppedAreaLeftPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.CroppedAreaLeftPixels"] = imageInfo.width;
    }

    pos = xmpData.findKey(Exiv2::XmpKey("Xmp.GPano.CroppedAreaTopPixels"));
    if (pos == xmpData.end()){
        xmpData["Xmp.GPano.CroppedAreaTopPixels"] = 0;
    }

    /*
    -ProjectionType=equirectangular 
    -FullPanoWidthPixels'<$ImageWidth' 
    -FullPanoHeightPixels'<$ImageHeight' 
    -CroppedAreaImageWidthPixels'<$ImageWidth' 
    -CroppedAreaImageHeightPixels'<$ImageHeight' 
    -CroppedAreaLeftPixels'<$ImageWidth' 
    -CroppedAreaTopPixels=0 
    -UsePanoramaViewer=True
    */
    if(!xmpData.empty())
        image->setXmpData(xmpData);
    if(!iptcData.empty())
        image->setIptcData(iptcData);
    image->writeMetadata();

}
