#include "parameter_parser.hpp"
#include <boost/program_options.hpp>
#include <iostream>
ParameterParser::ParameterParser()
{
    ifile = "default.png";
    ofile = "output.png";
    SourceType inputType = SourceType::TwoFishEyeParallelUpUp;
    bool showOriginal = false;
    float FoV = 220.0f;
    float xAxisRotation = 0.0f;
    float yAxisRotation = 0.0f;
    float zAxisRotation = 0.0f;
    float lLensRotation = 0.0f;
    float rLensRotation = 0.0f;
    
}

bool ParameterParser::parseParameters(int argc,char** argv, bool requireIFile, bool requireOFile){
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("version,v", "print version number")
        ("help", "produce help message")
        ("ifile", po::value<std::string>(), "input file")
        ("ofile", po::value<std::string>(), "output file")
        ("config",po::value<std::string>(), "configuration file in JSON format. Values can be overrided with regular flags")
        ("source-type",po::value<std::string>()->default_value("horizontal-fisheyes"), "Source type of image\n"
        "\tequilateral - Equilateral source image"
        "  fisheye - One fisheye lens"
        "  horizontal-fisheyes - Two fisheye lenses parallel"
        "  vertical-fisheyes - Two fisheye lenses one on top of the other"
        "  horizontal-fisheyes-top-right-left-mirrored - Two fisheye lenses parallel. Top of images to the right, Left mirrored")
        ("show-original","Show original image on preview window")
        ("preview-output","Show preview of result with possibility to change values from keyboard interactively.")
        ("fov",po::value<float>()->default_value(220.0),"FieldOfView of FishEyeLens. Calculated from edge of image. Over 180° is mixed linear with other image overlappping part.")
        ("x-axis-rotation",po::value<float>()->default_value(0.0),"Rotation of image on sphericalcoordinates along X-axis")
        ("y-axis-rotation",po::value<float>()->default_value(0.0),"Rotation of image on sphericalcoordinates along Y-axis")
        ("z-axis-rotation",po::value<float>()->default_value(0.0),"Rotation of image on sphericalcoordinates along Z-axis")
        ("l-lens-rotation",po::value<float>()->default_value(0.0),"Rotation of the image of the left lens around forward pointing vector")
        ("r-lens-rotation",po::value<float>()->default_value(0.0),"Rotation of the image of the right lens around forward pointing vector")
    ;

    //ExifInjector ei;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    
    if (vm.count("version,v")) {
        std::cout << version << "\n";
        return false;
    }
    if (vm.count("help")) {
        std::cout << desc << "\n";
        return false;
    }

    if ((!vm.count("ifile")&&requireIFile) || (!vm.count("ofile")&&requireOFile)) {
        std::cout << "You need to set input file -ifile and output file -ofile parameters" << '\n';
        return false;
    }else if(requireIFile || requireOFile){
        ifile = vm["ifile"].as<std::string>();
        ofile = vm["ofile"].as<std::string>();
    }

    if(vm.count("config")){
        //parseJSON

    }

    if (vm.count("source-type")){
        std::string sType = vm["source-type"].as<std::string>();
        if(sType == "equilateral"){
            inputType ==SourceType::Equirectangular;
        }else if(sType == "fisheye"){
            inputType ==SourceType::OneFishEye;
        }else if(sType == "horizontal-fisheyes"){
            inputType ==SourceType::TwoFishEyeParallelUpUp;
        }else if(sType == "vertical-fisheyes"){
            inputType ==SourceType::TwoFishEyeOneOnTopOther;
        }else if(sType == "horizontal-fisheyes-top-right-left-mirrored"){
            inputType ==SourceType::TwoFishEyeParallelUpRightLeftMirrored;
        }else{
            std::cout<<"invalid source-type parameter"<<std::endl<<desc<<std::endl;
            return false;
        }
    }
    return true;
}