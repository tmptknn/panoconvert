#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <string>
#include <limits.h>

#include <GLFW/glfw3.h>
#include <GL/glu.h>
#include "convertwithgl.hpp"
#include "sourcetypes.hpp"
#include "image_handling.hpp"
#include "exifinjector.hpp"
#include <thread>
int width, height;

GLFWwindow *window;
GLFWwindow *original_window;
GLWindowContext glwindow;
GLWindowContext originalglwindow;
ImageHandler imageHandler;
GLuint texture = 0;
GLuint original_texture = 0;
GLuint program = 0;
GLuint previewProgram = 0;
GLuint originalPreviewProgram = 0;
float xoffset = 0.0f;
float yoffset = 0.0f;
float zoffset = 0.0f;
float woffset = 180.0f;
float lLensRot = 0.0f;
float rLensRot = 0.0f;
SourceType currentSourceType = SourceType::TwoFishEyeParallelUpUp;
ExifInjector ei;
int windowwidth = 1600;
int windowheight = 800;
int originalwindowwidth = windowwidth/4;
int originalwindowheight = windowheight/4;
//unsigned char* data;
IMAGE_INFO incoming_image;
IMAGE_INFO output_image;

int outputwidth = 0;
int outputheight = 0;
std::string path="/tmp/";
void empty(float){
}

void emptyBool(bool){
}

void emptySourceType(SourceType type){
    ;
}


void (*xchanged)(float) = empty;
void (*ychanged)(float) = empty;
void (*zchanged)(float) = empty;
void (*fovchanged)(float) = empty;
void (*llensrotationchanged)(float) = empty;
void (*rlensrotationchanged)(float) = empty;
void (*sourceTypeChanged)(SourceType) = emptySourceType;


void setXChangedCallback(void (*func)(float)){
    xchanged=func;
    xchanged(xoffset);
}

void setYChangedCallback(void (*func)(float)){
    ychanged=func;
    ychanged(yoffset);
}

void setZChangedCallback(void (*func)(float)){
    zchanged=func;
    zchanged(zoffset);
}

void setFoVChangedCallback(void (*func)(float)){
    fovchanged=func;
    fovchanged(woffset);
}

void setLLensRotationCallback(void (*func)(float)){
    llensrotationchanged=func;
    llensrotationchanged(lLensRot);
}

void setRLensRotationCallback(void (*func)(float)){
    rlensrotationchanged=func;
    rlensrotationchanged(rLensRot);
}


void setSourceTypeCallback(void (*func)(SourceType)){
    sourceTypeChanged = func;
    sourceTypeChanged(currentSourceType);
}



void output_data(unsigned char *data, std::string filename, std::string originalfile)
{

    IMAGE_INFO info;
    info.data = data;
    info.width = outputwidth;
    info.height = outputheight;
    imageHandler.saveImage(filename, info);
    ei.readDataFromFile(originalfile);
    ei.writeDataToFile(filename, info);

}

GLuint loadProgramSourceType(SourceType type){

    std::string shader_name="fragmentshader.glsl";
    switch(type){
        case SourceType::Equirectangular:
            shader_name = "fragmentshaderequilateral.glsl";
            break;
        case SourceType::TwoFishEyeParallelUpUp:
            shader_name = "fragmentshadersamsung.glsl";
            fovchanged(woffset=193.5f);
            break;
        case SourceType::TwoFishEyeParallelUpRightLeftMirrored:
            // default shader
            fovchanged(woffset=216.0f);
            break;
        case SourceType::TwoFishEyeOneOnTopOther:
            shader_name = "fragmentshaderonetopoftheother.glsl";
            fovchanged(woffset = 198.5f);
            break;
        case SourceType::OneFishEye:
            shader_name = "fragmentshadersingle.glsl";
            fovchanged(woffset = 220.0f);
            break;

    }
    currentSourceType = type;
    return glwindow.loadProgram(path, shader_name);
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    if (key == GLFW_KEY_RIGHT  && action == GLFW_PRESS)
        xchanged(xoffset +=0.001);

    if (key == GLFW_KEY_LEFT  && action == GLFW_PRESS)
        xchanged(xoffset -=0.001);

    if (key == GLFW_KEY_UP  && action == GLFW_PRESS)
        ychanged(zoffset +=0.001);

    if (key == GLFW_KEY_DOWN  && action == GLFW_PRESS)
        ychanged(zoffset -=0.001);

    if (key == GLFW_KEY_PAGE_UP  && action == GLFW_PRESS)
        zchanged(yoffset +=0.001);

    if (key == GLFW_KEY_PAGE_DOWN  && action == GLFW_PRESS)
        zchanged(yoffset -=0.001);

    if (key == GLFW_KEY_N  && action == GLFW_PRESS)
        fovchanged(woffset +=0.1);

    if (key == GLFW_KEY_M  && action == GLFW_PRESS)
        fovchanged(woffset -=0.1);


    if (key == GLFW_KEY_1 && action == GLFW_PRESS){
        sourceTypeChanged(currentSourceType = SourceType::Equirectangular);
        program = loadProgramSourceType(currentSourceType);
    }

    if (key == GLFW_KEY_2 && action == GLFW_PRESS){
        sourceTypeChanged(currentSourceType = SourceType::TwoFishEyeParallelUpUp);
        program = loadProgramSourceType(currentSourceType);
    }

    if (key == GLFW_KEY_3 && action == GLFW_PRESS){
        sourceTypeChanged(currentSourceType = SourceType::TwoFishEyeParallelUpRightLeftMirrored);
        program = loadProgramSourceType(currentSourceType);
    }

    if (key == GLFW_KEY_4 && action == GLFW_PRESS){
        sourceTypeChanged(currentSourceType = SourceType::TwoFishEyeOneOnTopOther);
        program = loadProgramSourceType(currentSourceType);
    }

    if (key == GLFW_KEY_0 && action == GLFW_PRESS){
        sourceTypeChanged(currentSourceType = SourceType::OneFishEye);
        program = loadProgramSourceType(currentSourceType);
    }

    if (key == GLFW_KEY_O && action == GLFW_PRESS){
        lLensRot+=0.001;
        llensrotationchanged(lLensRot);
    }

    if (key == GLFW_KEY_P && action == GLFW_PRESS){
        lLensRot-=0.001;
        llensrotationchanged(lLensRot);
    }

    if (key == GLFW_KEY_U && action == GLFW_PRESS){
        rLensRot+=0.001;
        rlensrotationchanged(rLensRot);
    }

    if (key == GLFW_KEY_I && action == GLFW_PRESS){
        rLensRot-=0.001;
        rlensrotationchanged(rLensRot);
    }

}

static void error_callback(int error, const char *description)
{
    fprintf(stderr, "Error%i: %s\n", error, description);
}

void original_window_size_callback(GLFWwindow* window, int width, int height)
{
    originalwindowwidth = width;
    originalwindowheight = height;
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    windowwidth = width;
    windowheight = height;
}

void idleFunc(){
    glfwMakeContextCurrent(original_window);
    glClear(GL_COLOR_BUFFER_BIT);
    originalglwindow.drawOriginalData(original_texture, originalPreviewProgram, originalwindowwidth,originalwindowheight);
    glfwSwapBuffers(original_window);

    glfwMakeContextCurrent(window);
    glwindow.drawData(texture, program, outputwidth, outputheight, xoffset, yoffset, zoffset, woffset, lLensRot, rLensRot);
    glwindow.readInFrame((const unsigned char **)&output_image.data[0], outputwidth, outputheight);
    glClear(GL_COLOR_BUFFER_BIT);
    glwindow.previewData(previewProgram, windowwidth, windowheight);
    glfwSwapBuffers(window);
    glfwPollEvents();
}

int save(std::string name, std::string originalfile){

    std::thread saveimage1 = std::thread(output_data, output_image.data, name, originalfile);
    saveimage1.join();
    return 0;
}

int quit(){
    glfwDestroyWindow(window);
    glfwDestroyWindow(original_window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

void load(std::string ifilename){ //need to clean up memory beofre this. This leaks memory
    //row_pointers = nullptr;
    if(incoming_image.data != NULL) delete incoming_image.data;
    IMAGE_INFO info = imageHandler.loadImage(ifilename);
    incoming_image = info;
    width = info.width;
    height = info.height;

    originalwindowwidth = width/8;
    originalwindowheight = height/8;
    // data2 = new unsigned char[width * height * 4];

    //output_image.width = width;
    //output_image.height = height;
    //output_image.data = new unsigned char[width*height*4]; //is this correct still with jpg?
    bool equals = width==height;
    bool parallel = height < width;
    outputwidth = equals?width*2:parallel?width:height;
    outputheight = parallel?height:width;
    if(output_image.data != NULL) delete output_image.data;
    output_image.width = outputwidth;
    output_image.height = outputheight;
    output_image.data = new unsigned char[outputwidth * outputheight * 3];
    glfwMakeContextCurrent(window);
    //process_png_file(data);
    texture = glwindow.createTexture(incoming_image.data, width, height, outputwidth, outputheight);
    glwindow.addData((const unsigned char **)&incoming_image.data[0], width, height, texture);

    glfwMakeContextCurrent(original_window);
    glfwSetWindowSize(original_window, originalwindowwidth, originalwindowheight);
    original_texture = originalglwindow.createTexture(incoming_image.data, width,height,outputwidth, outputheight);
    originalglwindow.addData((const unsigned char **)&incoming_image.data[0], width, height, original_texture);
    originalglwindow.drawOriginalData(original_texture, originalPreviewProgram, originalwindowwidth, originalwindowheight);
    glfwSwapBuffers(original_window);
    glfwMakeContextCurrent(window);
    if(!parallel){
        sourceTypeChanged(SourceType::TwoFishEyeOneOnTopOther);
        program = loadProgramSourceType(SourceType::TwoFishEyeOneOnTopOther);
    }
    if(equals){
        sourceTypeChanged(SourceType::OneFishEye);
        program = loadProgramSourceType(SourceType::OneFishEye);
    }
    glfwSwapBuffers(window);

}

void start(int argc, char *argv[], std::string ifilename){
    check_gl_error();
    incoming_image.data = NULL;
    output_image.data =  NULL;
    char buf[PATH_MAX];
    char *res = realpath(argv[0], buf);
    (void)argc;                      /* make compiler happy */

    std::string whole_path =  std::string(buf);
    path = whole_path.substr(0,whole_path.find_last_of('/'));

    int numBytes = 0;


    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
    {
        fprintf(stderr, "ERROR: could not start GLFW3\n");

        exit(EXIT_FAILURE);
    }

    window = glwindow.init(windowwidth, windowheight);
    glfwSetWindowPos(window,originalwindowwidth,originalwindowheight);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwSetKeyCallback(window, key_callback);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glwindow.setupGL();


    program = loadProgramSourceType(currentSourceType);
    previewProgram = glwindow.setupPreviewProgram(path);

    original_window = originalglwindow.init(originalwindowwidth, originalwindowheight, "Original image");
    glfwSetWindowPos(original_window, 0, 0);
    glfwMakeContextCurrent(original_window);
    originalglwindow.setupGL();
    originalPreviewProgram = originalglwindow.setupPreviewProgram(path);
    load(ifilename);

    glfwSetWindowSizeCallback(original_window, original_window_size_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);

    glfwMakeContextCurrent(window);
    glwindow.drawData(texture, program, outputwidth, outputheight, xoffset, yoffset, zoffset, woffset, lLensRot, rLensRot);
    glwindow.readInFrame((const unsigned char **)&incoming_image.data[0], outputwidth, outputheight);

    glwindow.previewData(previewProgram, windowwidth, windowheight);
    glfwSwapBuffers(window);
    glfwFocusWindow(window);

}

void setXOffset(float value){
    xoffset = value;
}

void setYOffset(float value){
    yoffset = value;
}

void setZOffset(float value){
    zoffset = value;
}

void setFoV(float value){
    woffset = value;
}

void setLLensRotation(float value){
    lLensRot = value;
}

void setRLensRotation(float value){
    rLensRot = value;
}

void setProgramSourceType(SourceType sourceType){
    if(currentSourceType == sourceType) return;
    program = loadProgramSourceType(sourceType);
}

bool running(){
    return !glfwWindowShouldClose(window);
}



// compile with

// g++   g++ pngconverter.cpp convertwithgl.cpp -L/usr/lib/x86_64-linux-gnu -lpng -lGL -lglfw -lGLEW -lrt -lm -ldl -lXrandr -lXinerama -lXi -lXcursor -lXrender -ldl -ldrm -lXdamage -lXfixes -lX11-xcb -lxcb-glx -lxcb-dri2 -lXxf86vm -lXext -lX11 -lpthread -pthread -lxcb -lXau -lXdmcp -I/usr/includelibdrm -o pngconv
// add projection type with  exiftool -ProjectionType="equirectangular" x.jpg
// exiftool -ProjectionType=equirectangular -FullPanoWidthPixels'<$ImageWidth' -FullPanoHeightPixels'<$ImageHeight' -CroppedAreaImageWidthPixels'<$ImageWidth' -CroppedAreaImageHeightPixels'<$ImageHeight' -CroppedAreaLeftPixels'<$ImageWidth' -CroppedAreaTopPixels=0 -UsePanoramaViewer=True test1.jpg
