#include "pngfile.h"
#include "jpgfile.h"
#include "image_handling.hpp"
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <string>
ImageHandler::ImageHandler(){
    width = 0;
    height = 0;
    data = NULL;
}


IMAGE_INFO ImageHandler::loadImage(std::string fileName){
    if(boost::algorithm::ends_with(boost::algorithm::to_upper_copy(fileName), ".PNG"))
        return read_png_file(fileName.c_str());
    else
        return read_JPEG_file(fileName.c_str());
}

void ImageHandler::saveImage(std::string fileName, IMAGE_INFO info){
    if(boost::algorithm::ends_with(boost::algorithm::to_upper_copy(fileName), ".PNG"))
        write_png_file(fileName.c_str(), info);
    else
        write_JPEG_file(fileName.c_str(),info);
}